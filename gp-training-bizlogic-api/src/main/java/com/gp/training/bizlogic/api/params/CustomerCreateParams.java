package com.gp.training.bizlogic.api.params;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "customerCreateParams")
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerCreateParams {

	@XmlAttribute(name = "id")
	private int id;
	@XmlElement(name = "first_name")
	private String firstName;
	@XmlElement(name = "last_name")
	private String lastName;
	@XmlElement(name = "email")
	private String email;
	@XmlElement(name = "phone")
	private String phone;
	@XmlElement(name = "booking_id")
	private long bookingId;

	public static class Builder {
		private int id;
		private String firstName;
		private String lastName;
		private String email;
		private String phone;
		private long bookingId;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder firstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public Builder lastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public Builder email(String email) {
			this.email = email;
			return this;
		}

		public Builder phone(String phone) {
			this.phone = phone;
			return this;
		}

		public Builder bookingId(long bookingId) {
			this.bookingId = bookingId;
			return this;
		}
		public CustomerCreateParams build() {
			CustomerCreateParams p = new CustomerCreateParams();
			p.id = this.id;
			p.firstName = this.firstName;
			p.lastName = this.lastName;
			p.email = this.email;
			p.phone = this.phone;
			p.bookingId = this.bookingId;
			return p;
		}
	}

	public int getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public long getBookingId() {
		return bookingId;
	}
}

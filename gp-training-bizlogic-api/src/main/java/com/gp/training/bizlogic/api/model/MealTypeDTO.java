package com.gp.training.bizlogic.api.model;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "meal_type")
@XmlAccessorType(XmlAccessType.FIELD)
public class MealTypeDTO {

	@XmlAttribute(name = "id")
	private int id;
	@XmlElement(name = "code")
	private String code;
	@XmlElement(name = "name")
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

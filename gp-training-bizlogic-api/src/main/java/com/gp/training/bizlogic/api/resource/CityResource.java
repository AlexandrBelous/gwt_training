package com.gp.training.bizlogic.api.resource;

import java.util.List;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.gp.training.bizlogic.api.model.CityDTO;

@Path("/city")
public interface CityResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{cityId}")
	public CityDTO getCity(@PathParam("cityId") long cityId);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<CityDTO> getCities();

	@GET
	@Path("country")
	@Produces(MediaType.APPLICATION_JSON)
	List<CityDTO> getCitiesByCountry(@QueryParam("country") long id);
}

package com.gp.training.web.server.service;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.model.MealTypeDTO;
import com.gp.training.bizlogic.client.resource.DictionaryService;
import com.gp.training.web.server.utils.ProxyConverter;
import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.model.MealTypeProxy;
import com.gp.training.web.shared.service.DictionaryResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DictionaryResourceImpl implements DictionaryResource {

    @Autowired
    private DictionaryService dictionaryService;

    @Override
    public List<CountryProxy> getCountries() {
        List<CountryDTO> countries = dictionaryService.getCountries();

        List<CountryProxy> proxies = new ArrayList<>(countries.size());

        for (CountryDTO dto : countries) {
            CountryProxy proxy = ProxyConverter.convert2Proxy(dto);
            proxies.add(proxy);
        }

        return proxies;
    }

    @Override
    public List<CityProxy> getCities() {
        List<CityDTO> cities = dictionaryService.getCities();
        return convertCityDtosToProxies(cities);
    }

    @Override
    public List<CityProxy> getCitiesByCountry(Long id) {
        List<CityDTO> citiesByCountry = dictionaryService.getCitiesByCountry(id);
        return convertCityDtosToProxies(citiesByCountry);
    }

    @Override
    public List<MealTypeProxy> getMealTypes() {
        List<MealTypeDTO> meals = dictionaryService.getMeals();
        List<MealTypeProxy> proxies = new ArrayList<>(meals.size());
        for (MealTypeDTO dto : meals) {
            MealTypeProxy proxy = ProxyConverter.convert2Proxy(dto);
            proxies.add(proxy);
        }
        return proxies;
    }

    private List<CityProxy> convertCityDtosToProxies(List<CityDTO> dtos) {
        List<CityProxy> proxies = new ArrayList<>(dtos.size());
        for (CityDTO dto : dtos) {
            CityProxy proxy = ProxyConverter.convert2Proxy(dto);
            proxies.add(proxy);
        }

        return proxies;
    }
}

package com.gp.training.web.shared.model;

import java.util.Date;
import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class BookingCreateProxy {
	private int id;
	private int offerId;
	private int mealTypeId;
	private Date startDate;
	private Date endDate;
	private int guestCount;

	public BookingCreateProxy() {}

	public BookingCreateProxy(@MapsTo("id") int id,@MapsTo("offerId") int offer,@MapsTo("mealTypeId") int mealType,
	                    @MapsTo("startDate") Date startDate,@MapsTo("endDate") Date endDate,@MapsTo("guestCount") int guestCount) {
		this.id = id;
		this.offerId = offer;
		this.mealTypeId = mealType;
		this.startDate = startDate;
		this.endDate = endDate;
		this.guestCount = guestCount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOfferId() {
		return offerId;
	}

	public void setOfferId(int offerId) {
		this.offerId = offerId;
	}

	public int getMealTypeId() {
		return mealTypeId;
	}

	public void setMealTypeId(int mealTypeId) {
		this.mealTypeId = mealTypeId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}
}

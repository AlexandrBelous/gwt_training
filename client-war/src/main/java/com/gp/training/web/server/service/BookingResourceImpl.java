package com.gp.training.web.server.service;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.model.MealTypeDTO;
import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.client.resource.BookingService;
import com.gp.training.web.server.utils.ProxyConverter;
import com.gp.training.web.shared.model.BookingCreateProxy;
import com.gp.training.web.shared.model.BookingProxy;
import com.gp.training.web.shared.model.MealTypeProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.service.BookingResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookingResourceImpl implements BookingResource {

	@Autowired
	private BookingService bookingService;


	@Override
	public BookingProxy get(long id) {
		BookingDTO dto = bookingService.get(id);

		BookingProxy proxy = ProxyConverter.convert2Proxy(dto);
		return proxy;
	}



	@Override
	public BookingProxy create(BookingCreateProxy proxy) {
		BookingCreateParams params = new BookingCreateParams.Builder()
				.offerId(proxy.getOfferId())
				.mealType(proxy.getMealTypeId())
				.startDate(proxy.getStartDate())
				.endDate(proxy.getEndDate())
				.guestCount(proxy.getGuestCount())
				.build();

		BookingDTO dto = bookingService.create(params);
		BookingProxy booking = convertDto2ShortProxy(dto);
		return booking;
	}


	private BookingProxy convertDto2ShortProxy(BookingDTO dto) {
		BookingProxy p = new BookingProxy();
		p.setId(Integer.valueOf(String.valueOf(dto.getId())));
		return p;
	}


}

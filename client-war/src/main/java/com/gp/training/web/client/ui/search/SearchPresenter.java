package com.gp.training.web.client.ui.search;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.ui.common.Presenter;
import com.gp.training.web.shared.model.CityProxy;
import com.gp.training.web.shared.model.CountryProxy;
import com.gp.training.web.shared.model.OfferProxy;
import com.gp.training.web.shared.params.OfferSearchParams;

import java.util.List;

public interface SearchPresenter extends Presenter {
	
	void loadCountries(Callback<List<CountryProxy>, Void> callback);
	
	void loadCities(Callback<List<CityProxy>, Void> callback);
	
	void getOffers(OfferSearchParams params, Callback<List<OfferProxy>, Void> callback);

	void getCitiesByCountry(Long aLong, Callback<List<CityProxy>, Void> cityListCallback);
}

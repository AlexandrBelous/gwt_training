package com.gp.training.web.client.ui.booking;

import com.google.gwt.core.client.Callback;
import com.gp.training.web.client.ui.common.Presenter;
import com.gp.training.web.shared.model.BookingProxy;

public interface BookingPresenter extends Presenter {
	void getBooking(int bookingId, final Callback<BookingProxy, Void> bookingCallback);
}

package com.gp.training.web.shared.model;

import org.jboss.errai.common.client.api.annotations.MapsTo;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class MealTypeProxy {
	private int id;
	private String code;
	private String name;

	public MealTypeProxy() {}

	public MealTypeProxy(@MapsTo("id") int id,@MapsTo("code") String code,@MapsTo("name") String name) {
		this.id = id;
		this.code = code;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

package com.gp.training.web.client.ui.common;

import com.gp.training.web.client.params.PageParams;

public interface PageBaseView {
	void loadPageParams(PageParams params);
}

package com.gp.training.web.shared.service;

import com.gp.training.web.shared.model.BookingCreateProxy;
import com.gp.training.web.shared.model.BookingProxy;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/booking")
public interface BookingResource {

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public BookingProxy get(@PathParam("id")long id);

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public BookingProxy create(@RequestBody BookingCreateProxy proxy);
}

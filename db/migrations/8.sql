CREATE TABLE customers
(
  id INTEGER(11) primary key not null auto_increment,
  first_name varchar(25) not null,
  last_name varchar(60) not null,
  email varchar(255) not null,
  phone_number varchar(15) not null
);

create table booking_has_customers
(
  booking_id integer(11) not null,
  customer_id integer(11) not null,
  foreign key (booking_id) references booking(id),
  foreign key (customer_id) references customers(id)
);
package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.CustomerDTO;
import com.gp.training.bizlogic.api.params.CustomerCreateParams;

import java.util.List;

public interface CustomerService {

	CustomerDTO get(long id);
	CustomerDTO save(CustomerDTO dto);

	List<CustomerDTO> create(List<CustomerCreateParams> params);
}

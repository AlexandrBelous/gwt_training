package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.model.MealTypeDTO;
import com.gp.training.bizlogic.api.resource.CityResource;
import com.gp.training.bizlogic.api.resource.CountryResource;
import com.gp.training.bizlogic.api.resource.MealTypeResource;
import com.gp.training.bizlogic.client.ClientSender;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DictionaryServiceImpl implements DictionaryService {
	
	@Autowired
	private ClientSender sender;
	
	@Override
	public List<CountryDTO> getCountries() {		
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		CountryResource countryProxy = webTarget.proxy(CountryResource.class);
		List<CountryDTO> countries = countryProxy.getCountries();
		webTarget.getResteasyClient().close();
		return countries;
	}

	@Override
	public List<CityDTO> getCities() {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		CityResource cityProxy = webTarget.proxy(CityResource.class);
		List<CityDTO> cities = cityProxy.getCities();
		webTarget.getResteasyClient().close();
		return cities;
	}

	@Override
	public List<CityDTO> getCitiesByCountry(long id) {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		CityResource cityProxy = webTarget.proxy(CityResource.class);
		List<CityDTO> cities = cityProxy.getCitiesByCountry(id);
		webTarget.getResteasyClient().close();
		return cities;
	}

	@Override
	public List<MealTypeDTO> getMeals() {
		ResteasyWebTarget webTarget = sender.getRestEasyTarget();
		MealTypeResource mealTypeResource = webTarget.proxy(MealTypeResource.class);
		List<MealTypeDTO> meals = mealTypeResource.getAll();
		webTarget.getResteasyClient().close();
		return meals;
	}

}

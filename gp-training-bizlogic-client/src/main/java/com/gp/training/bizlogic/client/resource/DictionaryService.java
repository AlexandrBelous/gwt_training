package com.gp.training.bizlogic.client.resource;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.model.CountryDTO;
import com.gp.training.bizlogic.api.model.MealTypeDTO;

import java.util.List;

public interface DictionaryService {

    List<CountryDTO> getCountries();

    List<CityDTO> getCities();

    List<CityDTO> getCitiesByCountry(long id);

    List<MealTypeDTO> getMeals();
}

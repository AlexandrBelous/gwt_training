package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.Offer;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public class OfferDaoImpl extends GenericDaoImpl<Offer, Long> implements OfferDao {
	protected OfferDaoImpl() {
		super(Offer.class);
	}

}

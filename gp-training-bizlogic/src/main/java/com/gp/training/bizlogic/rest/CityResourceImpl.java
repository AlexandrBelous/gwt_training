package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.api.model.CityDTO;
import com.gp.training.bizlogic.api.resource.CityResource;
import com.gp.training.bizlogic.dao.CityDao;
import com.gp.training.bizlogic.domain.City;
import com.gp.training.bizlogic.utils.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CityResourceImpl implements CityResource {
	
	@Autowired
	private CityDao cityDao;
	
	@Override
	public CityDTO getCity(long cityId) {
		City city = cityDao.get(Long.valueOf(cityId));
		return DtoConverter.convert2Dto(city);
	}
	
	@Override
	public List<CityDTO> getCities() {
		List<City> cities = cityDao.getAll();
		
		List<CityDTO> cityDtos = new ArrayList<>(cities.size());
		for (City city : cities) {
			CityDTO dto = DtoConverter.convert2Dto(city);
			cityDtos.add(dto);
		}
		
		return cityDtos;
	}

	@Override
	public List<CityDTO> getCitiesByCountry(long id) {
		List<City> cities = cityDao.getByCountry(id);

		List<CityDTO> cityDtos = new ArrayList<>(cities.size());
		for (City city : cities) {
			CityDTO dto = DtoConverter.convert2Dto(city);
			cityDtos.add(dto);
		}

		return cityDtos;
	}


}

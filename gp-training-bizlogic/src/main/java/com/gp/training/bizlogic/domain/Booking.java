package com.gp.training.bizlogic.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="booking")
public class Booking extends AbstractEntity {
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="offer_id")
	private Offer offer;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "booking_has_customers",
			joinColumns = @JoinColumn(name = "booking_id"),
			inverseJoinColumns = @JoinColumn(name = "customer_id"))
	private List<Customer> customers = new ArrayList<>();

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "meal_type_id")
	private MealType mealType;

	@Column(name = "guest_count")
	private int guestCount;

	@Column(name="start_date")
	private Date startDate;
	@Column(name = "end_date")
	private Date endDate;

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public MealType getMealType() {
		return mealType;
	}

	public void setMealType(MealType mealType) {
		this.mealType = mealType;
	}

	public int getGuestCount() {
		return guestCount;
	}

	public void setGuestCount(int guestCount) {
		this.guestCount = guestCount;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}

package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.domain.Customer;
import com.gp.training.bizlogic.domain.MealType;
import com.gp.training.bizlogic.domain.Offer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import com.gp.training.bizlogic.domain.Booking;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BookingDaoImpl extends GenericDaoImpl<Booking, Long> implements BookingDao {
	private final static String CREATE_BOOKING =
			"Insert into training.booking(offer_id, meal_type_id, start_date, end_date, guest_count) " +
					"values (?, ?, ?, ?, ?);";

	public BookingDaoImpl() {
		super(Booking.class);
	}

	@Autowired
	private JdbcTemplate template;

	@Override
	@Transactional(readOnly = false)
	public Booking create(BookingCreateParams params) {

		Booking entity = new Booking();
		Offer o = new Offer();
		o.setId(params.getOfferId());
		MealType m = new MealType();
		m.setId(params.getMealTypeId());
		entity.setOffer(o);
		entity.setMealType(m);
		entity.setGuestCount(params.getGuestCount());
		entity.setStartDate(params.getStartDate());
		entity.setEndDate(params.getEndDate());
		Booking b = em.merge(entity);

		return b;
	}

	@Override
	public Booking get(long id) {
		Booking entity = em.find(Booking.class, id);
		return entity;
	}

	private Map<String, Object> convertParams(BookingCreateParams params) {
		Map<String, Object> map = new HashMap<>();
		map.put("offer", params.getOfferId());
		map.put("meal", params.getMealTypeId());
		map.put("startDate", new Date(params.getStartDate().getTime()));
		map.put("endDate", new Date(params.getEndDate().getTime()));
		map.put("guests", params.getGuestCount());
		return map;
	}
}

package com.gp.training.bizlogic.domain;

import javax.persistence.*;

@Entity
@Table(name = "offer")
public class Offer extends AbstractEntity {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "hotel_id")
	private Hotel hotel;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "room_type_id")
	private RoomType roomType;

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}
}

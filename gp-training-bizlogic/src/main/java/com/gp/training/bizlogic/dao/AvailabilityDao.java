package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.model.OfferDTO;
import com.gp.training.bizlogic.params.AvailSearchParams;

import java.util.List;

public interface AvailabilityDao {
	
	public List<OfferDTO> findOffers(AvailSearchParams params);

	OfferDTO get(long id);
}

package com.gp.training.bizlogic.rest;

import com.gp.training.bizlogic.domain.Booking;
import com.gp.training.bizlogic.utils.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gp.training.bizlogic.api.model.BookingDTO;
import com.gp.training.bizlogic.api.params.BookingCreateParams;
import com.gp.training.bizlogic.api.resource.BookingResource;
import com.gp.training.bizlogic.dao.BookingDao;

@Component
public class BookingResourceImpl implements BookingResource {

	@Autowired
	private BookingDao bookingDao;

	@Override
	public BookingDTO create(BookingCreateParams params) {

		Booking entity = bookingDao.create(params);
		BookingDTO dto = new BookingDTO();
		dto.setId(entity.getId());
		return dto;
	}

	@Override
	public BookingDTO get(long id) {
		Booking entity = bookingDao.get(id);
		BookingDTO dto = DtoConverter.convert2Dto(entity);
		return dto;
	}




}

package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.domain.City;

import java.util.List;

public interface CityDao extends GenericDao<City, Long> {

    List<City> getByCountry(long id);
}

package com.gp.training.bizlogic.dao;

import com.gp.training.bizlogic.api.params.CustomerCreateParams;
import com.gp.training.bizlogic.domain.Customer;

import java.util.List;

public interface CustomerDao extends GenericDao<Customer, Long> {

    Customer save(Customer customer);

    List<Customer> save(List<CustomerCreateParams> params);
}
